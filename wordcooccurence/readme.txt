Task2 - Word coocurence counter

Word Co-Occurrence measures thefrequency with which two words appear close to each other in a corpus of documents
– For some definition of ‘near’

Mapper:
-------
map(docid a, doc d) {
	foreach w in d do
		foreach u near w do emit(pair(w, u), 1)
}

Reducer is normal SumReducer the same as in previous example (already filled for you)
-------

'Near' function 
-------------

In our academical case treat near as neightboring words -> for each word on the line except for the first word, emit an output record that has "previous word, this word" as a pair in mentioned algorithm above (to simplify the problem to create pair don't build separated serializable object and use string concatenation inside Text object)


TODO:
=====
* driver code
* mapper code
* junit Tests
* execution on hadoop pseduo cluster for 'bible' as input data
