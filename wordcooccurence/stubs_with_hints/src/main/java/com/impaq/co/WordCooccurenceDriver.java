package com.impaq.co;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;

public class WordCooccurenceDriver {

  public static void main(String[] args) throws Exception {
    if (args.length != 2) {
      System.out.printf(
          "Usage: WordCooccurence <input dir> <output dir>\n");
      System.exit(-1);
    }

    /*
     * Instantiate a Job object for your job's configuration.  
     */
    Job job = new Job();
    
    /**
     * Specify the jar file that contains your driver, mapper, and reducer.
     * Hadoop will transfer this jar file to nodes in your cluster running
     * mapper and reducer tasks.
     */
//    job.setJarByClass(..);
    
    /**
     * Specify an easily-decipherable name for the job.
     * This job name will appear in reports and logs.
     */
//    job.setJobName(..args.);

    /**
     * Specify the paths to the input and output data based on the
     * command-line arguments.
     */
//    FileInputFormat.setInputPaths(job, ...);
//    FileOutputFormat.setOutputPath(job, ...);

    /**
     * Specify the mapper and reducer classes.
     */
//    job.setMapperClass(...);
//    job.setReducerClass(...);

    /**
     * Specify the mapper and reduce output key and value classes.
     */
//    job.setMapOutputKeyClass(...)
//    job.setMapOutputValueClass(...)
//    job.setOutputKeyClass(...);
//    job.setOutputValueClass(...);

    /**
     * Start the MapReduce job and wait for it to finish.
     */
    boolean success = job.waitForCompletion(true);
    System.exit(success ? 0 : 1);
  }
}
