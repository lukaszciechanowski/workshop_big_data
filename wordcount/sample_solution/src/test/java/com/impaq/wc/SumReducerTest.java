package com.impaq.wc;

import java.util.Arrays;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class SumReducerTest {

	IntWritable one = new IntWritable(1);

	ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;

	@Before
	public void setup() {
		/**
		 * Setup your reduceDriver object and set reducer to test
		 */
		SumReducer sumReducer = new SumReducer();
		reduceDriver = new ReduceDriver<Text, IntWritable, Text, IntWritable>();
		reduceDriver.setReducer(sumReducer);
	}

	@Test
	public void shouldSumWordOccurence() {
		// given
		/**
		 * Specify what you expect as input for reducer 
		 * NOTICE: single word and a list of "ones" symbolic occurrence
		 */
		String sampleWord = "hadoop";
		reduceDriver.withInput(new Text(sampleWord), Arrays.asList(one, one, one));

		// then 
		/**
		 * Specify what you expect as output for reducer 
		 * NOTICE: single word and occurrence sum
		 */
		reduceDriver.withOutput(new Text(sampleWord), new IntWritable(3));
		
		// when
		reduceDriver.runTest();
	}

}
