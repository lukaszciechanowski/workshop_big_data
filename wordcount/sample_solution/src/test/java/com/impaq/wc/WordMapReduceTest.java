package com.impaq.wc;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class WordMapReduceTest {

	IntWritable one = new IntWritable(1);

	MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

	@Before
	public void setup() {
		/**
		 * Setup your reduceDriver object and set mapper and reducer to test
		 */
		WordMapper wordMapper = new WordMapper();
		SumReducer sumReducer = new SumReducer();
		mapReduceDriver = new MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable>();
		mapReduceDriver.setMapper(wordMapper);
		mapReduceDriver.setReducer(sumReducer);
	}
	
	@Test
	public void shouldCountWords() {

		// given 
		/**
		 * Specify what you expect as input for job
		 * NOTICE: set of text lines with lines numbers (actually offset ..)
		 */
		mapReduceDriver.withInput(new LongWritable(1) , new Text("ala ma kota"));
		mapReduceDriver.withInput(new LongWritable(2) , new Text("ola ma psa"));
		
		// then
		/**
		 * Specify what you expect as output for job 
		 * NOTICE: single word and a word count for each word. Remember of sorted key order!
		 */
		mapReduceDriver.withOutput(new Text("ala"), one);
		mapReduceDriver.withOutput(new Text("kota"), one);
		mapReduceDriver.withOutput(new Text("ma"), new IntWritable(2));
		mapReduceDriver.withOutput(new Text("ola"), one);
		mapReduceDriver.withOutput(new Text("psa"), one);
		
		// when
		mapReduceDriver.runTest();
	}
}
