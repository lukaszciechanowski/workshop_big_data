package com.impaq.wc;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

public class WordMapperTest {

	IntWritable one = new IntWritable(1);

	MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;

	@Before
	public void setup() {
		/**
		 * Setup your reduceDriver object and set mapper to test
		 */
		WordMapper wordMapper = new WordMapper();
		mapDriver = new MapDriver<LongWritable, Text, Text, IntWritable>();
		mapDriver.setMapper(wordMapper);
	}
	
	@Test
	public void shouldEmmitOnesWorEachWord() {

		// given 
		/**
		 * Specify what you expect as input for mapper 
		 * NOTICE: Line number (offset) and single line of text
		 */
		mapDriver.withInput(new LongWritable(1) , new Text("ala ma kota"));
		
		// then
		/**
		 * Specify what you expect as output for mapper 
		 * NOTICE: single word and a "ones" emitted for each word
		 */
		mapDriver.withOutput(new Text("ala"), one);
		mapDriver.withOutput(new Text("ma"), one);
		mapDriver.withOutput(new Text("kota"), one);

		// when
		mapDriver.runTest();
	}
}
