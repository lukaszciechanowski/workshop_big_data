Task1 - Word count (Mapper and Reducer):

1) Import the project: "stubs_with_hints" to eclipse
2) Fill the Mapper and Reducer regarding to hints. Driver code and tests are already prepared for you
3) Build jar file: mvn clean install
4) Unzip sample data: tar zxvf bible.tar.gz 
5) Copy sample data to HDFS: hadoop fs -put bible/all-bible bible
6) Check if bible was properly copied: hadoop fs -ls
7) Run hadoop job in folder where the jar was created: hadoop jar word-count-1.0.jar com.impaq.wc.WordCountDriver bible output
8) Check if output was generated: hadoop fs -ls output
9) Check the output: hadoop fs -cat output/part-r-00000 | tail


